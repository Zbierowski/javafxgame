package images;

public class TwoNumbers {
    private int first = -1;
    private int second = -1;

    public int getFirst() {
        return first;
    }

    public int getSecond() {
        return second;
    }

    public void setFirst(int first) {
        this.first = first;
    }

    public void setSecond(int second) {
        this.second = second;
    }

    public boolean checkIfEquals(){
        return getFirst() == getSecond();
    }

    @Override
    public String toString() {
        return this.first + ", " + this.second;
    }
}
