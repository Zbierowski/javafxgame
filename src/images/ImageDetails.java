package images;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class ImageDetails {

    private final StringProperty image_url;
    private final StringProperty title;
    private final StringProperty image_url_2;
    private final StringProperty title_2;

    public ImageDetails(String image_url, String title, String image_url_2, String title_2){
        this.image_url = new SimpleStringProperty(image_url);
        this.title = new SimpleStringProperty(title);
        this.image_url_2 = new SimpleStringProperty(image_url_2);
        this.title_2 = new SimpleStringProperty(title_2);
    }

    public String getImage_url() {
        return image_url.get();
    }

    public String getImage_url_2() {
        return image_url_2.get();
    }

    public String getTitle() {
        return title.get();
    }

    public String getTitle_2() {
        return title_2.get();
    }

    @Override
    public String toString() {
        return this.title + " " + this.image_url + ", " + this.title_2 + " " + this.image_url_2;
    }
}
