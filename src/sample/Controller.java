package sample;

import connection.ConnectionClass;
import images.ImageDetails;
import images.TwoNumbers;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.net.URL;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Random;
import java.util.ResourceBundle;

public class Controller implements Initializable {

    @FXML
    private Label questionLabel;

    @FXML
    private Label roundLabel;

    @FXML
    private Button backButton;

    @FXML
    private Button nextButton;

    @FXML
    private ImageView imageLeft;

    @FXML
    private ImageView imageRight;

    @FXML
    private Label imageLabelLeft;

    @FXML
    private Label imageLabelRight;

    private ArrayList<Round> rounds = new ArrayList<Round>();
    private static int amount;
    private static int currentRound = 0;
    private static int currentQuestion = 0;
    private static int question = 0;
    private ObservableList<ImageDetails> data;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        rounds.add(new Round(1, 5));
        rounds.add(new Round(2, 7));
        rounds.add(new Round(3, 9-2));
        this.setQuestion();
        this.setRound();
        this.backButton.setText("Wstecz");
        this.backButton.setDisable(true);
        this.nextButton.setText("Następny");
        this.amount = this.rounds.get(this.currentRound).getQuestions();
        this.getImages();
        this.imageLeft.setImage(new Image(this.data.get(this.question).getImage_url()));
        this.imageRight.setImage(new Image(this.data.get(this.question).getImage_url_2()));
        this.imageLabelLeft.setText(this.data.get(this.question).getTitle());
        this.imageLabelRight.setText(this.data.get(this.question).getTitle_2());
    }

    public void nextQuestion(ActionEvent actionEvent) {
        Platform.runLater(() -> {
            if(this.currentQuestion == 0) {
                this.backButton.setDisable(false);
            }

            if(this.currentRound == this.rounds.size() - 1 && this.currentQuestion == this.amount-1) {
                this.nextButton.setDisable(true);
            }

            System.out.println("size " + this.rounds.size());

            if(this.currentRound < this.rounds.size()) {
                if(this.currentQuestion < this.amount){
                    this.currentQuestion++;
                    this.setQuestion();
                } else {
                    this.backButton.setDisable(true);
                    this.currentQuestion = 0;
                    this.setQuestion();
                    this.currentRound++;
                    this.setRound();
                    if(this.currentRound < this.rounds.size()) {
                        this.amount = this.rounds.get(this.currentRound).getQuestions();
                    }
                }

                this.question++;
                if(this.question < this.data.size()) {
                    String qsc[] = new String[2];
                    qsc[0] = this.data.get(this.question).getImage_url();
                    qsc[1] = this.data.get(this.question).getImage_url_2();

                    TwoNumbers num = this.getTwoNumbers(1);

                    this.imageLeft.setImage(new Image(qsc[num.getFirst()]));
                    this.imageRight.setImage(new Image(qsc[num.getSecond()]));

                    this.imageLabelLeft.setText(this.data.get(this.question).getTitle());
                    this.imageLabelRight.setText(this.data.get(this.question).getTitle_2());
                    System.out.println("sss " + this.data.get(this.question).getImage_url_2());
                }
            } else {
                System.out.println("Koniec");
            }
        });
    }

    public void backQuestion(ActionEvent actionEvent) {

        if (this.currentQuestion == 1) {
            this.backButton.setDisable(true);
        }

        if(this.currentQuestion > 0) {
            this.currentQuestion--;
            this.setQuestion();
            this.question--;

            this.imageLeft.setImage(new Image(this.data.get(this.question).getImage_url()));
            this.imageRight.setImage(new Image(this.data.get(this.question).getImage_url_2()));
            this.imageLabelLeft.setText(this.data.get(this.question).getTitle());
            this.imageLabelRight.setText(this.data.get(this.question).getTitle_2());

            System.out.println("Q: " + this.data.get(this.question).getImage_url() + ", " + this.data.get(this.question).getImage_url_2());
        }
    }

    public void setQuestion() {
        this.questionLabel.setText("Pytanie " + (this.currentQuestion + 1));
    }

    public void setRound () {
        this.roundLabel.setText("Runda" + (this.currentRound + 1));
    }

    public void getImages() {
        data = FXCollections.observableArrayList();
        ConnectionClass cc = new ConnectionClass();
        cc.getConnection();

        try {
            ResultSet rs = cc.getConnection().createStatement().executeQuery("select * from images");
            while(rs.next()) {
                data.add(new ImageDetails(
                        rs.getString("image_url_1"), rs.getString("title"),
                        rs.getString("image_url_2"), rs.getString("title2")));
            }
        } catch(Exception ex) {
            ex.printStackTrace();
        }

    }

    private TwoNumbers getTwoNumbers(int size) {
        TwoNumbers twoNumbers = new TwoNumbers();

        do {
            twoNumbers.setFirst(this.getRand(0, size));
            twoNumbers.setSecond(this.getRand(0, size));
        } while (twoNumbers.checkIfEquals());

        return twoNumbers;
    }


    private int getRand(int min, int max) {
        Random rand = new Random();
        return rand.nextInt((max - min) + 1) + min;
    }
}
