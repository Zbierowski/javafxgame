package sample;

public class Round {
    private int orderNo;
    private int questions;

    public Round (int orderNo, int questions) {
        this.orderNo = orderNo;
        this.questions = questions;
    }

    public int getOrderNo() {
        return orderNo;
    }

    public int getQuestions() {
        return questions;
    }
}
