package connection;

import java.sql.Connection;
import java.sql.DriverManager;

public class ConnectionClass {

    public Connection connection;
    public Connection getConnection() {

        String dbName="game";
        String userName="root";
        String password="root";

        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            connection= DriverManager.getConnection("jdbc:mysql://localhost:3306/game?user=root&password=root&useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC");
            System.out.println("Set a connection " + connection.getCatalog());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return connection;
    }
}
